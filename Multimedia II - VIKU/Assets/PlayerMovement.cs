﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public CharacterController2D controller;

    float horizontalMove = 0f;

    public float runSpeed = 40f;
    bool jump = false;
    bool doubleJump = false;
    bool canStillJump = false;

    bool crouch = false;

    // Update is called once per frame
    void Update()
    {
        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;

        if (Input.GetButtonDown("Jump"))
        {
            if (controller.isGrounded())
            {
                Debug.Log("jump");
                jump = true;
                canStillJump = true;    
                
            }
            else if (canStillJump)
            {
                Debug.Log("doubleJump");
                doubleJump = true;
                canStillJump = false;
            }
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
        } else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }
    }

    //Mouvements du personnage
    void FixedUpdate ()
    {
        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump, doubleJump);
        
        jump = false;
        doubleJump = false;
    }
}
